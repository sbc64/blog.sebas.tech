---
title: Fork You
date: 2019-11-16
---

The meaning of Fork You
<!-- more -->

Recently I was inspired the movie `The Gambler` and its famous quote of: "That leaves you in a fortress of solitude. In a position of fuck you."

So what is Fork You? Just basically what open source software is. "Want to implement a new feature? Fork you! I don't have time for that. You can go do it yourself". Neovim vim is basically the natural outcome of Fork you.

Nothing special here. Just a few thoughts about open source software.