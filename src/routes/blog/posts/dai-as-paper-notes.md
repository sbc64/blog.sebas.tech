---
title: DAI as paper notes
date: 2019-09-16T12:51:00.000Z
---

An idea about how to create paper notes for DAI.

<!-- more -->

## <a name="top"></a> Table of Contents

* [Banknotes](#Banknotes)
* [Why cryptonotes](#Why)
* [How to create cryptonotes](#How)
* [Fighting fraudulent notes](#Fighting)
* [Unnecessary](#Unnecessary)
* [Kong Notes](#Kong)

***

I didn't care much about money. Nor did I care much about bills and coins until my grandfather gave me a coin collection when I was young and ever since then I've been collecting coins here and there. One of my biggest splurges in buying coins was when I was buying them in Brazil and I got some coins that dates back to the 1700s. Big coins worth 1 Portuguese Real. I kept collecting coins and always assume that coins where backed by gold and were also backed by central banks with the force of their military.

In recent years, with my involvement with crypto currency, I have learned that money can be anything we humans decide to give it value to. It doesn't matter what it is so long as it is fungible, fraud proof, and trustworthy. There are many explanations of what money is which I won't into because many other people have delved into the topic and debate almost every single point, but I do recommend that you start out by reading:

- [Shelling out](https://web.archive.org/web/20190902061853/https://nakamotoinstitute.org/shelling-out/)

And for more mind blowing literature I recommend:

- [Debt: The First 500 years](https://www.amazon.com/Debt-Updated-Expanded-First-Years/dp/1612194192)
- [The Ascent of Money](https://www.amazon.com/Ascent-Money-Financial-History-World/dp/0143116177)

Briefly said, money has to satisfy certain properties and certain things can be money. During the first years of Banking created in England, private bank would issue their own bank notes that would IOUs (I owe you) a certain amount of gold. This practice continued for many years until the 1900s, when central banks basically took over of all of the countries finances and created their own fiat currency.

# <a name="Banknotes"></a>Cryptonotes

I really want cryptocurrencies to become the most widely used form of money. Ethereum has a huge potential for this (link to ryan adams eth is money article). To the surprise of many, crypto currency can be made into physical cash by creating [crypto notes](https://cryptoticker.io/en/bitcoin-notes-launched-by-tangem/). The problem with this approach is that the private key is on the bill itself. A scammer can somehow extract the private key, spend the bill, and then drain the bitcoin out of the bill. I haven't personally tried to this and I would love someone to show me that it is not possible, but I think if you give me a bank note with a private key on it for claiming the crypto on it... then I will most certainly find a way to take as much as I can.

[polymere note](https://imgur.com/a/bEJqy#UDVmXnx)


# <a name="Why"></a>Why cryptonotes?

Notes are a useful way of transacting value without anything digital. It really is an issue that when the there is no electricity or internet one simply cannot perform digital transactions (there are ways but not easily usable to non tech users). Cryptonotes solve this because you can just use them like cash now a days. The cryptonotes represent an IOU to a certain amount of crypto. A 5 DAI note will be 5 DAI of crypto. People would learn to trust these crypto notes because they know they are backed. There will always be forgery but I think there are ways of preventing it.

One of the biggest benefits of creating a cryptonote is that it is anonymous crypto currency. This is one of the biggest benefits.

To sum the benefits up:

- Physical IOUs (When the internet is out one can still do commerce)
- Anonymous


# <a name="How"></a>How to create one.

What are the requirements for a crypto note to work?

First of all, it needs to be an IOU, and people will need trust this IOU. Why will people trust the IOU note? Because it will have a system for redeeming the crypto in it similar to how Argent HQ guardians work or how Shamir secret sharing works. These guardians can either be banks or people located in other places of the world. The crypto note will have its crypto locked in a multisig sig wallet of sorts. This prevents individuals from draining the value it contains. 

The note cannot contain the private key on it or else people will find it easy to counterfeit it. This is why it has to be an IOU. If the had the private key on it then it would just be a coin itself, like gold. It will also be of upmost importance that fraudulent bills don't enter the system or else this could severely damage the trust in the crypto notes. When someone wants to redeem the note they can go to trusted issuers. These issuers would ideally destroy the note and ask all the other issuers to release the crypto stored in the note.

The notes also cost money to produce. It can't be done through charity work. It needs real funding. Maybe the notes can be denominated in DAI but it will use gDAI or cDAI in the multi sig contract. This way it can generate revenue in the form of interest to the multisig signers. This revenue can be managed through a DAO, but the easier solution would just be to keep earning interest on the notes. When a user redeems the note it can 


# <a name="Fighting"></a>Fighting fraudulent notes.

I think one can fight fraudulent notes using two systems. One can scan the qr code to see that the note still has the value in it. This way shops can quickly verify that the note has not been drained because in order for the note to be drained it would need.

One can also do a longer computation and ask a smart contract to check if the note has been transferred recently. This contract will use zksnarks to save transactions that the note has done. Sortha like a checkpoint/savepoint of a note's history. Why is this useful? A user can check using zero knowledge if the note has had a duplicate transaction in the past (double spent). But because of the way zero knowledge works... there is no way of knowing who did the transaction and for what. It just works more like a savepoint that the note does with the contract. When someone wants to redeem the note the savepoint will automatically be executed. Maybe this is extra overhead since one can check the value in the note.

The whole system looks as following:

- The DAI value in the notes is locked in a multisig with guardians.
- The DAI is put into compound finance to pay for 


# <a name="Unnecessary"></a>Unnecessary

Crypto is awesome. But there is a huge portion of the population that will never use digital forms and maybe might not have access to internet or capable smart phones. I hope that the newer cheap end nokias can get lower means people access to.

These cryptonotes will most likely be unnecessary when the whole world is using WalletConnect and connext payment channels. It is still an interesting idea for me just because it is the merger of banknotes and crypto currency which are two of my obsession.

# <a name="Kong"></a>Kong Notes

I just recently learned about [Kong Notes](kong.cash). This is most certainly something I should use as an example to follow.

[[Top]](#top)
