---
title: Coming Collapse of the Age of Technology
date: 2019-10-01T21:51:08
---

A little-noticed event of exceptional importance occurred on the
8th of May, 1998. The conservative, power-oriented champion of
science, progress, and reason, Science magazine, published an article
by the distinguished British scientist James Lovelock which said...

<!-- more -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title></title>
	<meta name="generator" content="LibreOffice 6.3.1.2 (Linux)"/>
	<meta name="created" content="2019-10-01T21:51:08.968329687"/>
	<meta name="changed" content="2019-10-01T21:53:09.468659412"/>
	<style type="text/css">
		@page { size: 8.5in 11in; margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 115%; background: transparent }
		strong { font-weight: bold }
		em { font-style: italic }
		a:link { color: #000080; so-language: zxx; text-decoration: underline }
	</style>
</head>
<body lang="en-US" link="#000080" vlink="#800000" dir="ltr"><p><strong>The
Coming Collapse of the Age of Technology</strong></p>
<p>David Ehrenfeld</p>
<p>A little-noticed event of exceptional importance occurred on the
8th of May, 1998. The conservative, power-oriented champion of
science, progress, and reason, Science magazine, published an article
by the distinguished British scientist James Lovelock which said:</p>
<p>“We have confidence in our science-based civilization and think
it has tenure. In so doing, I think we fail to distinguish between
the life-span of civilizations and that of our species. In fact,
civilizations are ephemeral compared with species.”</p>
<p>Lovelock, originator of the Gaia Hypothesis—about the central
role of life in the earth’s self-regulating system that includes
atmosphere, climate, land, and oceans—went on to recommend that we
“encapsulate the essential information that is the basis of our
civilization to preserve it through a dark age.” The book would be
written not on ephemeral, digital magnetic, or optical media but on
“durable paper with long-lasting print.” It would record in
simple terms our present knowledge of science and technology,
including the fundamentals of medicine, chemistry, engineering,
thermodynamics, and natural selection. As the monasteries did in the
Dark Ages, the book would help to keep our culture from vanishing
during a prolonged period of chaos and upheaval.</p>
<p>Set aside the question of whether such a task could be done, or
whether science ought to be described for future generations in a
neutral way. What commands our attention first is that <em>Science</em>
magazine was willing to print two precious pages based on the premise
that our scientific-technological civilization is in real danger of
collapse.<br/>
<strong><br/>
Can the Machine Stop?</strong></p>
<p>Nearly everyone in our society, experts and lay people alike,
assumes that the events and trends of the immediate future—the next
five to twenty-five years—are going to be much like those of the
present. We can do our business as usual. In the world at large,
there will be a continued increase in global economic, social, and
environmental management; a continued decrease in the importance of
national and local governments compared with transnational
corporations and trade organizations; more sophisticated processing,
transfer, and storage of information; more computerized management
systems along with generally decreased employment in most fields;
increased corporate consolidation; and a resulting increase in the
uniformity of products, lifestyles, and cultures. The future will be
manifestly similar to today.</p>
<p>Power carries with it an air of assured permanence that no
warnings of history or ecology can dispel. As John Ralston Saul has
written, “Nothing seems more permanent than a long-established
government about to lose power, nothing more invincible than a grand
army on the morning of its annihilation.” The present
economic-technical-organizational structure of the industrial and
most of the non-industrial world is the most powerful in history.
Regardless of one’s political orientation, it’s very difficult to
imagine any other system, centralized or decentralized, ever
replacing it. Reinforcing this feeling is the fact that our
technology-driven economic system has all the trappings of royalty
and empire, without the emperor. It rolls on inexorably, a giant
impersonal machine, devouring and processing the world, unstoppable.</p>
<p>Futurists of all political varieties, those who fear and loathe
the growing power as well as those who welcome it, share faith in its
permanence. Even those who are aware of the earth’s growing social
and environmental disasters have this faith. Robert D. Kaplan
originally writing in the <em>Atlantic Monthly</em> in 1994, is an
example. “We are entering a bifurcated world,” said Kaplan, in
“The Coming Anarchy.” Part of it, in West Africa, the Indian
subcontinent, Central America, and elsewhere in the underdeveloped
world, will be subject to ethnic conflict, food scarcity, massive
overcrowding, militant fundamentalism, the breakdown of national
governments and conventional armies, and the resurgence of epidemic
disease, all against a backdrop of global climatic change. But the
other part of the world will be “healthy, well-fed, and pampered by
technology.” We’ll be all right, those of us with the money and
the technology. The system will not fail us.</p>
<p>Despite the grip of the idea of irreversible progress on the
modern mind, there are still some people who believe in the cyclical
view of history. Have they generated a different scenario of the
future? Not necessarily. The archaeologist Joseph Tainter notes in
his book, <em>The Collapse of Complex Societies</em>, that collapse
and disintegration have been the rule for complex civilizations in
the past. There comes a time when every complex social and political
system requires so much investment of time, effort, and resources
just to keep itself together that it can no longer be afforded by its
citizens. Collapse comes when, first, a society “invests ever more
heavily in a strategy that yields proportionately less” and,
second, when “parts of a society perceive increasing advantage to a
policy of separation or disintegration.”</p>
<p>Forget the Mayan and Roman Empires: what about our own? Certainly
the problem of spending more and getting less describes our present
condition. Are we receiving full value from an international banking
and finance system that shores up global speculators with billions of
dollars of public money, no matter how recklessly they gamble and
whether they win or lose? Our NAFTA strategy has cost this country
tens of thousands of jobs, reduced our food security, and thrown our
neighbor, Mexico, into social, economic, and environmental turmoil;
is this an adequate repayment for the dollars and time we have spent
on free trade? More than 70 percent of government-supported research
and development is spent on weapons that yield no social, and a
highly questionable military, benefit; the Pentagon loses—misplaces—a
billion dollars worth of arms and equipment each year. Is this a
profitable investment of public funds? We may ask whether the decline
in returns on investment in this system has reached the critical
point. Tainter quotes from a popular sign: “Every time history
repeats itself the price goes up.” The price is now astronomical.</p>
<p>If we follow Tainter, however, we need not worry about our future.
In the curiously evasive final chapter of his book, he states that
“Collapse today is neither an option nor an immediate threat.”
Why not? Because the entire world is part of the same complex system.
Collapse will be prevented, in effect, by everyone leaning on
everyone else. It reminds me of that remote island, described by the
great British humorist P.G. Wodehouse, where the entire population
earned a modest but comfortable living by taking in each other’s
washing.</p>
<p>I don’t have this kind of blind faith. I don’t believe in the
permanence of our power. I doubt whether the completely globalized,
totally managed, centralized world is going to happen.
Techno-economic globalization is nearing its apogee; the system is
self-destructing. There is only a short but very damaging period of
expansion left.</p>
<p>Now if I were playing it comparatively safe, I would stick to the
more obvious kinds of support for my argument, the things I know
about as an ecologist. I would write about our growing environmental
problems, especially certain kinds of pollution and ecosystem
destabilization: global soil erosion; global deforestation; pollution
and salinization of freshwater aquifers; desertification; saline
seeps, like those that have ruined so much prime agricultural land in
Australia; growing worldwide resistance of insects to insecticides;
acid rain and snow; on-farm transfer of genes for herbicide
resistance from crops to weeds; the loss of crop varieties; the
collapse of world fisheries; the decline, especially in Europe, of
mycorrhizal fungi needed for tree growth; the effects of increasing
co2 and introduced chemicals in the atmosphere, including but not
limited to global warming; the hole in the ozone layer; the
extinction and impending extinction of keystone species such as the
pollinators needed for the propagation of so many of our crops and
wild plants; the accelerated spread of deleterious exotic species
such as the Asian tiger mosquito; the emergence of new, ecologically
influenced diseases, and the resurgence of old diseases, including,
for example, the recent discovery of locally transmitted malaria in
New Jersey, New York City, Michigan, Toronto, California, and Texas;
the spread of antibiotic resistance among pathogenic bacteria; and
finally the catastrophic growth of the human population, far
exceeding the earth’s carrying capacity—all of these things
associated with the techno-economic system now in place.</p>
<p>Some of the problems I mentioned are conjectural, some are not;
some are controversial, some are not; but even if only half or a
fifth of them materialize as intractable problems, that will be quite
enough to bring down this technological power structure.</p>
<p><strong>The Forces of Internal Breakdown</strong></p>
<p>But I am not going to dwell on the ecological side effects of our
technology, important as they are; most of them have already received
at least some attention. I am leaving this comparatively safe turf to
discuss the forces of internal breakdown that are inherent in the
very structure of the machine. Part of the system’s power comes
from our faith in its internal strength and cohesiveness, our bland
and infuriating confidence that somebody is at the wheel, and that
the steering and brakes are working well.</p>
<p>The causes of the problems affecting our global system are
numerous, overlapping, and often obscure—I will not try to identify
them. The problems themselves, however, are clear enough. I have
grouped them in six broad categories.<br/>
<strong><br/>
1. The
Misuse of Information</strong></p>
<p>One of the most serious challenges to our prevailing system is our
catastrophic loss of ability to use self-criticism and feedback to
correct our actions when they place us in danger or give bad results.
We seem unable to look objectively at our own failures and to adjust
the behavior that caused them. I’ll start with three examples.
First observation: in 1997, NASA launched the Cassini space probe to
Saturn. After orbiting the earth, it is programmed to swing around
Venus to gain velocity, then head back toward earth at tremendous
speed, grazing us, if all control thrusters function exactly as
planned, at a distance of only 312 miles, using our gravity to
accelerate the probe still more and turn it into a Saturn-bound
trajectory. The space probe cost $3.5 billion and carries in its
nuclear energy cell seventy-two pounds of plutonium-238, the most
deadly substance in existence. Alan Kohn, former
emergency-preparedness operations officer at the Kennedy Space
Center, described Cassini as “criminally insane.” Yet this
dramatic criticism from a NASA insider, plus similar concerns
expressed by many outside scientists, did not stop the project.</p>
<p>The second example: on February 15, 1996, President Clinton
launched his Technology Literacy Challenge, a $2 billion program
which he hoped would put multimedia computers with fiber optic links
in every classroom. “More Americans in all walks of life will have
more chances to live up to their dreams than in any other time in our
nation’s history,” said the president. He singled out a
sixth-grade classroom in Concord, New Hampshire, where students were
using Macintosh computers to produce a very attractive school
newspaper. Selecting two editorials for special notice, he praised
the teacher for “the remarkable work he has done.” An article in
New Jersey’s Star Ledger of February 26, 1996 gave samples of the
writing in those editorials. The editorial on rainforest destruction
began: “Why people cut them down?” The editorial about the
president’s fights with Congress said, “Conflicts can be very
frustrating. Though, you should try to stay away from conflicts.…
In the past there has been fights.”</p>
<p>The third example: around the world, funds are being diverted away
from enormously successful, inexpensive methods of pest control, such
as the use of beneficial insects to attack pests, to the costly,
risky, and unproven technologies favored by multinational,
biotechnology corporations. Hans Herren, whose research in the
biological control of the insect pests of cassava helped avert a
famine threatening 200 million Africans, said: “When I visit
[African] agricultural research institutes, I find the biological
control lab half empty, with broken windows … but the biotechnology
lab will be brand new with all the latest equipment and teeming with
staff.”</p>
<p>These examples, superficially quite different, show that we are
not using the information at hand about the results of our past
actions to guide and direct what we plan to do next. This inability
to correct ourselves when we go astray is exacerbated by the
dangerously high speed of our decision-making (Jeremy Rifkin calls it
the “nanosecond culture”), a consequence of modern,
computer-assisted communications. This speed short-circuits the
evolutionary process of reasoned decision-making, eliminating time
for empirical feedbacks and measured judgment. Messages arriving by
express mail, fax, and email all cry out for an immediate response.
Often it is better to get a night’s sleep before answering.</p>
<p>A final example of the misuse of information is information glut.
We assume these days that information is like money: you can’t have
too much of it. But, in fact, too much information is at least as bad
as too little: it masks ignorance, buries important facts, and
incapacitates minds by overwhelming the critical capacity for
brilliant selectivity that characterizes the human brain. That
quantity and quality are so often inversely related in today’s
information flow compounds this problem. If our feedback alarm bells
were sounding properly, we would curtail the flow of junk—instead,
we worship it.</p>
<p><strong>2. The Loss of Information</strong></p>
<p>The acceleration of obsolescence is a plague afflicting all users
of contemporary technology. Although obsolescence is an inherent part
of any technology that isn’t moribund, several factors have
combined in the last few decades to exaggerate it out of manageable
proportions. One factor is the sheer number of people involved in
technology, especially information technology—each has to change
something or make something new to justify a salary. Another factor
is the market’s insistence on steadily increasing sales, which in
turn mandates an accelerated regimen of planned obsolescence.</p>
<p>The social disruption caused by accelerated obsolescence is well
known. A less familiar, yet equally important, result is the loss of
valuable knowledge. The technical side of this was described by Jeff
Rothenberg in an article in the January 1995 issue of <em>Scientific
American</em>, entitled “Ensuring the Longevity of Digital
Documents.” It turns out that neither the hardware nor the software
that underlie the information revolution has much staying power. “It
is only slightly facetious,” says Rothenberg, “to say that
digital information lasts forever—or five years, whichever comes
first.” The most durable digital storage medium, the optical disk,
has a physical lifetime of only thirty years and an estimated time to
obsolescence of ten years. Digital documents are evolving rapidly,
and shifts in their basic form are frequent. Translation backwards or
forwards in time becomes difficult, tedious, and expensive—or
impossible. The result is the loss of much of each previous
generation’s work, a generation being defined as five to twenty
years. There is always something “better” coming; as soon as it
arrives, we forget all about it.</p>
<p>One striking example of the obsolescence nightmare, documented by
Nicholson Baker in <em>The New Yorker</em> and by Clifford Stoll in
his book <em>Silicon Snake Oil</em>, concerns the widespread
conversion of paper library card catalogs to electronic ones. Having
spent a fortune to convert their catalogs, libraries now find
themselves in an electronic-economic Catch-22. The new catalogs don’t
work very well for many purposes, and the paper catalogs have been
frozen or destroyed. Better electronic systems are always on the
horizon. Consequently, libraries spend a third or more of their
budgets on expensive upgrades of software and hardware, leaving
little money for books and journals.</p>
<p>A second example of the effects of obsolescence is the wholesale
forgetting of useful skills and knowledge—everything from how to
operate a lathe to how to identify different species of earthworms.
Whole branches of learning are disappearing from the universities.
The machine is jettisoning both knowledge and diversity (a special
kind of information) simultaneously. To illustrate the loss of
biodiversity, biologists Stephen Hall and John Ruane have shown that
the higher the GNP in the different countries of Europe—the more
integrated into “the system” they are—the higher the percentage
of extinct breeds of livestock. I’m sure that the same relationship
could be shown for agricultural crop varieties or endangered
languages. The system is erasing our inheritance.</p>
<p>Another problem involving the loss of information is incessant
reorganization, made easier by information technology and causing
frequent disruption of established social relationships among people
who work and live together. Changes occur too rapidly and too often
to permit social evolution to work properly in business, in
government, in education, or in anything touched by them.</p>
<p>An article by Dirk Johnson in the “Money and Business” section
of <em>The New York Times</em> of March 22, 1998 described some
recent problems of the Leo Burnett advertising agency, which gave the
world the Jolly Green Giant and the Marlboro Man. Johnson described
one especially serious trouble for a company that prides itself on
its long-term relationships with clients: “No one at Burnett can do
much about a corporate world that shuttles chief executives in and
out like managers on a George Steinbrenner team and that has an
attention span that focuses on nothing older than the last earnings
report. It is not easy to build client loyalty in such a culture, as
many other shops can attest.”</p>
<p><strong>3. Increasing Complexity and Centralized Control</strong></p>
<p>A third intrinsic problem with the techno-economic system is its
increasing complexity and centralized control, features of much of
what we create—from financial networks to nuclear power plants.
Nature, with its tropical rainforests, temperate prairies, and marine
ecosystems, is also complex. But nature’s slowly evolved complexity
usually involves great redundancy, with duplication of functions,
alternative pathways, and countless, self-regulating, fail-safe
features. Our artificial complexity is very different: it is marked
by a high degree of interlinkage among many components with little
redundancy, by fail-safe mechanisms that are themselves complex and
failure-prone, and by centralized controllers who can never fully
comprehend the innumerable events and interactions they are supposed
to be managing. Thus, our artificial systems are especially
vulnerable to serious disturbances. System-wide failures—what the
Yale sociologist Charles Perrow calls “normal accidents”—occur
when one component malfunctions, bringing down many others that are
linked in ways that are poorly observed and understood. The fruits of
this complexity and linkage are everywhere, from catastrophic
accidents at chemical and nuclear plants to the myriad effects of
accelerated climatic change.</p>
<p>Accidents and catastrophes are the most noticeable results of
running a system that is beyond our full understanding, but the more
routine consequences, those that don’t necessarily make the
front-page headlines, may be more important. Many of these
consequences stem from a phenomenon first described by John von
Neumann and Oskar Morgenstern in 1947, and applied to social systems
by the biologist Garrett Hardin in 1968. Von Neumann and Morgenstern
pointed out that it is mathematically impossible to maximize more
than one variable in an interlinked, managed system at any particular
time. If one variable is adjusted to its maximum, it limits the
freedom to maximize other variables—in other words, in a complex
system we cannot make everything “best” simultaneously. As we
watch economists desperately juggling stock prices, wages, commodity
prices, productivity, currency values, national debts, employment,
interest rates, and technological investments on a global scale,
trying to maximize them all, we might think about von Neumann’s and
Morgenstern’s theory and its implications for the fate of our
complex but poorly redundant techno-economic machine.<br/>
<strong><br/>
4.
Confusing Simulation with Reality</strong></p>
<p>The fourth group of problems is the blurring of the distinction
between simulation and reality. With globalization, the ease of
making large changes in a simulation on the computer screen is
accompanied by a corresponding ease of ordering these large changes
in the real world—with disastrous results in activities as
different as the planning of massive public works projects, the
setting of monetary policy, and the conduct of war. Beginning with
the Vietnam war, all contemporary American military adventures have
had this through-the-looking-glass quality, in which the strategies
and simulations conform poorly to actual events on the ground. As
Martin van Creveld shows in his book, <em>The Transformation of War</em>,
the simulated war of the military strategists is increasingly
different from the realities of shifting, regional battlefields with
their “low-intensity conflicts” against which high-tech weapons
and classic military strategies are often worse than useless.</p>
<p>As we attempt to exert more complicated controls over our world,
more modeling, with its assumptions and simplifications, is needed.
This in turn causes all kinds of errors, some serious, most hidden.
According to James Lovelock, years before the ozone hole was
discovered by a lone pair of British observers using an old-fashioned
and inexpensive instrument, it was observed, measured, and ignored by
very expensive satellite-borne instruments which had been programmed
to reject data that were substantially different from values
predicted by an atmospheric model. Simulation had triumphed over
reality.</p>
<p>What I call the “pseudocommunity problem” is another
illustration of the fuzzy line that exists between simulation and
reality. It began with television, which surrounded viewers with
friends they did not have and immersed them in events in which they
did not participate. The philosopher Bruce Wilshire, an articulate
and charismatic lecturer, has observed that students who are
otherwise polite and attentive talk openly and unselfconsciously
during his lectures, much as if he were a figure on a television
screen who could not be affected by their conversation.</p>
<p>Email and the Internet have made this situation much worse. Email
has opened up a world of global communications that has attracted
many of our brightest and most creative citizens, especially young
people. Considerable good has come of this—for the handicapped, for
those who have urgent need of communicating with people in distant
places, for those living in politically repressed countries, and
others. But the ease and speed of email are traps that few evade.
Real human interaction requires time, attention to detail, and work.
There is a wealth of subtlety in direct conversation, from body
language to nuances of voice to choice of words. In email this
subtlety is lost, reduced to the level of the smiley face used to
indicate a joke. The result is a superficial, slipshod substitute for
effective communication, often marked by careless use of language and
hasty thought. Every hour spent online in the “global village” is
an hour not spent in the real environment of our own communities. It
is an hour of not experiencing the love and support of good
neighbors; an hour of not learning how to cope with bad neighbors,
who cannot be erased by a keystroke; an hour of not becoming familiar
with the physical and living environment in which we actually live.
Perhaps this is why a recent study of the social involvement and
psychological well-being of Internet users, published in American
Psychologist, found a significant decrease in the size of their
social circle and a significant increase in their depression and
loneliness after one to two years online. There are no good
substitutes for reality.</p>
<p><strong>5. The Unnecessary Exhaustion of Resources</strong></p>
<p>Our techno-economic system is distinguished by its exceptionally
high consumption of renewable and non-renewable resources. When this
is pointed out, advocates of the system answer that substitutes for
depleted resources will be found or new technologies will eliminate
the need for them. To date, neither of these claims has been
demonstrated to be true in any significant case. Meanwhile,
resources—from food to forests, from fresh water to soil—are
disappearing quickly. Blindness to warnings of impending shortage
limits our options: by not responding while there are still the time
and resources left to take corrective action, we are forced to work
under crisis conditions, when there is little that can be done. The
problem is too familiar to require much elaboration, but the most
conspicuous example deserves brief mention. The global production of
oil will probably peak—and thereafter decline—sometime between
2000 and 2010, regardless of new oil field development. Almost every
part of our technology, including nuclear technology, depends on oil.
Oil is not going to disappear any time soon. But we have already used
more than half of the world’s supply, with most of this consumption
in the last few decades. As the recent report of Petroconsultants S.
A. and the book, <em>The Coming Oil Crisis</em>, by the distinguished
petroleum geologist C. J. Campbell make plain, we have only a very
few years left of cheap oil.</p>
<p>The loss of cheap oil will strike far more deeply than can be
predicted by economists’ price-supply curves; it will fatally
damage the stability of the transnational corporations that run our
global techno-economic system. Transnational corporations are,
ultimately, economic losers. Too often they rely on the sale of
products that don’t work well and don’t last, that are made in
unnecessarily expensive ways (usually as a result of making them very
quickly), that are expensively transported, carry high environmental
and human costs, and are purchased on credit made available by the
seller. At present, these products are subsidized by subservient,
lobbyist-corrupted governments through tax revenues and favorable
regulation; their flaws are concealed by expensive advertising
promotions which have co-opted language and human behavioral
responses in the service of sales; and they are imposed on consumers
by the expensive expedient of suppressing alternative choices,
especially local alternatives. All of this depends on the
manipulation of a huge amount of surplus wealth by the
transnationals, wealth that has been generated by cheap oil. When the
oil becomes expensive, with no comparably inexpensive energy
substitutes likely, when jobs disappear and the tax base shrinks,
when consumers become an endangered species, and when corporate
profits dwindle and the market values of their stocks decline, the
fundamental diseconomies of global corporations will finally take
their toll and we will begin to see the transnationals disintegrate.</p>
<p><strong>6. The Loss of Higher Inspiration</strong></p>
<p>There is one final, internal problem of the system, maybe the most
important: namely, a totally reductionist, managed world is a world
without its highest inspiration. With no recognized higher power
other than the human-made system that the people in charge now
worship, there can be no imitation of God, no vision of something
greater to strive for. Human invention becomes narrow, pedestrian,
and shoddy; we lose our best models for making lasting, worthy
societies. One such model is the noble dream of people and their
communities functioning non-destructively, justly, and democratically
within a moral order. The other—long a reality—is nature itself,
whose magnificent durability we will never totally comprehend, but
which has much to teach us if we want to learn. When people and
communities become mere management units and nature is only something
to exploit, what is left worth striving after? We become no better
than our machines, and just as disposable.</p>
<p><strong>The End of Global Management</strong></p>
<p>The reductionist idea of a fully explainable and manageable world
is a very poor model of reality by any objective standard. The real
world comprises a few islands of limited understanding in an endless
sea of mystery. Any human system that works and survives must
recognize this. A bad model gives bad results. We have adopted a bad
model and now we are living with the terrible consequences.<br/>
The
present global power system is a transient, terminal phase in a
process that began 500 years ago with the emerging Age of Reason. It
has reached its zenith in the twentieth century, powered by the
global arms trade and war and enabled by a soulless, greed-based
economics together with a hastily developed and uniquely dangerous
technology. This power system, with its transnational corporations,
its giant military machines, its globalized financial system and
trade, its agribusiness replacing agriculture—with its growing
numbers of jobless people and people in bad jobs, with its endless
refugees, its trail of damaged cultures and ecosystems, and its fatal
internal flaws, is now coming apart. Realization of the machine’s
mortality is the necessary first step before we begin to plan and
work for something better. As the great British philosopher Mary
Midgley says, “The house is on fire; we must wake up from this
dream and do something about it.”</p>
<p>Looming over us is an ominous conjunction of the internal sources
of breakdown I have just described with the many, interlinked
ecological and social threats that I only briefly listed. What can we
do? Obviously a crash as comprehensive as the one that’s coming
will affect all of us, but that doesn’t mean that there is nothing
that can be done to soften the blow. We should begin by accepting the
possibility that the system will fail. While others continue to sing
and dance wildly at the bottom of the avalanche slope, we can chose
to leave the insane party.</p>
<p>I do not mean going back to some prior state or period of history
that was allegedly better than the world today. Even if going back
were possible, there is no halcyon period that I would want to
regain. Nor do I mean isolating ourselves in supposedly
avalanche-proof shelters—gated communities of like-minded
idealists. No such shelter could last for long; nor would such an
isolated existence be desirable. In the words of my friend,
geographer Meg Holden, we should be unwilling “to admit defeat in
the wager of the Enlightenment that people can create a nation based
not on familial, racial, ethnic, or class ties, but on … the
betterment of self only through the betterment of one’s fellow
citizens.” There is no alternative but to move forward—a task
that will place the highest demands on our ability to innovate and on
our humanity.</p>
<p>Moving forward requires that we provide satisfying alternatives to
those who have been most seriously injured by the present technology
and economics. They include farmers, blue-collar workers suddenly
jobless because of unfair competition from foreign slave labor or
American “workfare,” and countless souls whose lives and work
have been made redundant by the megastores in the shopping malls. If
good alternatives are not found soon, the coming collapse will
inevitably provoke a terrible wave of violence born of desperation.</p>
<p><strong>Creating a Shadow System</strong></p>
<p>Our first task is to create a shadow economic, social, and even
technological structure that will be ready to take over as the
existing system fails. Shadow strategies are not new, and they are
perfectly legal. An illustration is Winston Churchill’s role in
Britain before the start of World War II. Churchill was a member of
the governing Conservative Party but was denied power, so he formed
his own shadow organization within the party. During the 1930s, while
Hitler was rearming Germany and the Conservative leadership was
pretending that nothing was happening, Churchill spoke out about the
war he knew was coming, and developed his own plans and alliances.
When Hitler’s paratroopers landed in Holland and Belgium in 1940,
and Prime Minister Neville Chamberlain’s silk umbrella could not
ward them off, Churchill was chosen by popular acclaim to replace him
as prime minister. He created a dynamic war cabinet almost overnight,
thanks to his shadow organization.</p>
<p>The shadow structure to replace the existing system will comprise
many elements, with varying mixes of the practical and theoretical.
These elements are springing up independently, although linkages
among them are beginning to appear. I will give only two examples;
both are in the early stages of trial and error development.</p>
<p>The first is the rapid growth of community-sponsored agriculture
(CSA) and the return of urban farmers’ markets. In CSAs, farmers
and local consumers are linked personally by formal agreements that
guarantee the farmers a timely income, paid before the growing
season, and the consumers a regular supply of wholesome, locally
grown, often organic, produce. The first CSA project in the United
States was started in 1985, in western Massachusetts, by the late
Robyn Van En—just thirteen years later, there are more than 600
CSAs with over 100,000 members throughout the United States. Urban
farmers’ markets similarly bring city-dwellers into contact with
the people who grow their food, for the benefit of both. Although
difficulties abound—economic constraints for farmers whose CSAs
lack enough members, the unavailability of the benefits of CSAs to
the urban poor, who do not have cash to advance for subsequent
deliveries of produce—creative solutions seem possible. A related
development has been the burgeoning of urban vegetable gardening in
cities across the country. One of the most exciting examples is the
garden project started by Cathrine Sneed for inmates of the San
Francisco Jail and subsequently expanded into the surrounding urban
community.</p>
<p>On another front, less local and immediate but equally important,
is the embryonic movement to redefine the rights of corporations,
especially to limit the much-abused legal fiction of their
“personhood.” The movement would take away from corporations the
personal protections granted to individuals under the U.S.
Constitution and Bill of Rights. The Constitution does not mention
corporations; state charter laws originally made it plain that
corporations could exist and do business only through the continuous
consent of state governments. If a corporation violated the public
trust, its charter could be revoked. The loss of the public right to
revoke charters of corporations and the emergence of the corporation
as an entity with limited liability and the property and other
personal rights of citizens, was a tragic and often corrupt chapter
in nineteenth-century American law. It has led to our present
condition, in which transnational corporations with no local or
national allegiances control many of the government’s major
functions, subverting democracy and doing much to create the unstable
conditions I have described.</p>
<p>Recapturing the government’s right to issue and cancel corporate
charters should be a primary goal of those trying to build a more
durable and decent social, economic, and technical system. Michael
Lerner, editor of <em>Tikkun</em>, has suggested that we add a Social
Responsibility Amendment to the Constitution containing the key
provision that each corporation with annual revenues of $20 million
or more must receive a new corporate charter every twenty years.
Similar ideas are being advanced by Richard Grossman and Ward
Morehouse of the Program on Corporations, Law &amp; Democracy; by
Peter Montague, editor of <em>Rachel’s Environment &amp; Health
Weekly</em>; and by others in the United States and Canada. Although
still embryonic, the movement has drawn support from both
conservatives and liberals—the shadow structure is neither of the
right nor the left, but is an emerging political alliance that may
gain power when the transnationals decline.</p>
<p>In the words of Vaclav Havel, president of the Czech Republic,
spoken in Philadelphia’s Independence Hall on July 4, 1994: “There
are good reasons for suggesting that the modern age has ended.… It
is as if something were crumbling, decaying and exhausting itself,
while something else, still indistinct, were arising from the
rubble.” What is crumbling is not only our pretentious
techno-economic system but our naive faith in our ability to control
and manage simultaneously all the animate and inanimate functions of
this planet. What is arising—I hope in time—is a new spirit and
system rooted in love of community, and love of the land and nature
that sustain community. And the greatest challenge will be to make
this spirit and system truly new and truly enduring by finding ways
to develop our love of nature and community without returning to
destructive nationalisms, without losing our post-Enlightenment
concern for the common good of the rest of humankind and nature.</p>
<p>David Ehrenfeld, professor of Biology at Rutgers University, is
the author of <em>The Arrogance of Humanism </em>(Oxford, 1978) and
<em>Beginning Again </em>(Oxford, 1993). He is the founding editor of
the international scientific journal <em>Conservation Biology</em>.</p>
<p>– – – – –<br/>
© 1999-2003 <em>Tikkun Magazine</em>.
This article may be found on the web
at:<br/>
<a href="http://www.tikkun.org/magazine/index.cfm/action/tikkun/issue/tik9901/article/990111a.html">http://www.tikkun.org/magazine/index.cfm/action/tikkun/issue/tik9901/article/990111a.html</a><br/>
This
article may be reproduced for purposes of personal scholarship only.
For other uses, please contact <em>Tikkun</em> at:<br/>
2342 Shattuck
Avenue, Suite 1200, Berkeley, CA 94704<br/>
Phone: (510) 644-1200 |
Fax: (510) 644-1255 | Email: <a href="mailto:magazine@tikkun.org">magazine@tikkun.org</a></p>
</body>
