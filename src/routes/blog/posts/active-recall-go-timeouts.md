---
title: Go HTTP timeouts
date: 2020-01-27
---

I found this great article about go's `net/http`...

<!-- more -->


I found this great article about go's [net/http](https://web.archive.org/web/20200107001735/https://ieftimov.com/post/make-resilient-golang-net-http-servers-using-timeouts-deadlines-context-cancellation/) explaining about its timeouts. This post is an effort to actively recall what I learned. A la [A mind for numbers](https://blog.sebas.tech/blog/focus/#books).


Things that I learned:

- That the [context](https://golang.org/pkg/context/) is a way of making sure a requests gets cancelled or redirected in a timely matter. Each handler request will have its own context.
- The concept of the different read and write timeouts. One wants to make sure that when the client cancels a requests that the server doesn't continue working to call the API or process the request.