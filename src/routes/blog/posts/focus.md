---
title: Focus
date: 2020-01-10
---

I was inspired by blog post I read.

<!-- more -->

I was inspired by a blog post I read. This one [https://xojoc.pw/blog/focus](https://xojoc.pw/blog/focus). It got me thinking about doing something similar but a bit more encompassing of my life as whole. Focusing on a tech stack is something that I noticed I needed after last year. Last year I experimented with several different technologies and it gave me a good lay of the tech land. At the same time I was spread far too thin in my abilities and didn't get the deep knowledge that makes me an expert in the area.

It was a great year and I learned a lot, but now I'm going to be more focused about what I read to get a solid foundation. I felt that last year I was too scattered. I went after shiny new things. This year is going to be on regaining my focus and this post is a promise to myself to focus more this 2020.

## Technology tools I will use

<p></p>

### NixOS/DWM

[NixOs](https://nixos.org) has a solid ground with its ease of replication across different environments. It is most likely an overkill for many situations but I think learning about NixOS will pay off in the future. For my desktop environment I'm going to stick to [DWM](https://dwm.suckless.org/). I have already spent many years using dwm and changing will be counter productive

#### Vscode

I have been a huge fan of [neovim](https://neovim.io/) just because I really try to be a minimalist with my computer resources. It is certainly a huge pet peeve of mine, but getting a good coding environment with neovim takes too much time. I will instead spend this time on other things and will from now on use [vsode](https://code.visualstudio.com/) for all my programming needs.

#### Python

Python is the first language I truly learned and I think it is most certainly one of the most dynamics languages out there. Being good at python will allow one to work on almost anything.

#### Go vs Rust

I chose `go` instead of `rust`. I dabbled a bit in Rust last year and it was great to learn about it. Go is a strong contender in the realm of WASM and [great people are working on it](https://christine.website/blog/wasmcloud-progress-2019-12-08). I also believe that Go is one of the best languages to work in teams. If you look at most Go code repositories they are all fairly homogeneous. This makes collaboration much better. In contrast with Rust which can be extremely variant, and verbose.

One draw back of Go is that [memory bugs are the biggest source of critical vulnerabilities](https://msrc-blog.microsoft.com/2019/07/16/a-proactive-approach-to-more-secure-code/). If WASM is the future then I should figure out what are the memory safety considerations of Go or Rust running in a WASM environment. Go uses runtime garbage collection which adds a bigger binary. Rust is different, it doesn't have garbage collection built in to the program but instead has an extremely aggressive compiler that forces the user to build memory safe code. I will need to think about this more... I really want Rust to be the language I use and I will probably fall back to Rust in the future.

EDIT: 2020-01-13
**Just saw this [video](https://web.archive.org/web/20200213235317/https://www.youtube.com/watch?v=-2BO43FvBuA). Another video making me FOMO into Rust. The money shot? "People are picking Rust to do WASM things is the same way as Go got big after Docker existed since Docker is mostly written in Docker. It just makes sense that cloud tooling is written in the same language as Docker is written in. WASM is doing the same thing.**

#### Svelte

I'm not a strong front end dev. I will most certainly focus only on Typescript, not Javascript. Svelte has smaller builds. It doesn't have the React Virtual DOM. In the field I work in, which is blockchain, decentralized applications might have unreliable performance so my thesis is that having smaller builds will make dApps more efficient.

#### PostgreSQL/Redis

Same reason as the original author wrote which is: "Postgres is the best general implementation of SQL. Redis is a lightweight in-memory Key-value store and message broker."

#### DevOps

I'm going to focus mostly on `docker stack` and `ansible`. Kubernetes seems like it will be useful but I don't think I will have the bandwidth to learn how to use it. Hopefully something like WasmOps comes out _wink_ _wink_: [Birth and Death of Javascript](https://web.archive.org/web/20200108111901/https://www.destroyallsoftware.com/talks/the-birth-and-death-of-javascript), [If WASM had existed](https://web.archive.org/web/20190331040119/https://twitter.com/solomonstre/status/1111004913222324225), and [WASMCloud](https://web.archive.org/web/20191225062113/https://christine.website/blog/wasmcloud-progress-2019-12-08).

## Books

Last year I read 15 books. Not extremely prolific but better than my previous year. This isn't counting the blog posts I read last year, which there are also quite a few. I once read this phrase: **"If you want to learn something, read it twice."** and this has stuck with me ever since. This year, instead of trying to cram everything into my head as fast as possible, I'm going to focus on a few books which I hope will give me a good foundation about the world.

- [A Guide to the Good Life: The Ancient Art of Stoic Joy](https://www.amazon.com/Guide-Good-Life-Ancient-Stoic/dp/1522632735): [Meditations](https://www.amazon.com/Meditations-Marcus-Aurelius/dp/1503280462) was a strong contender for this spot, but A Guide to the Good Life has better applicable advice.

- [A Mind for Numbers](https://www.amazon.com/Mind-Numbers-Science-Flunked-Algebra-ebook/dp/B00G3L19ZU): This book has a great part about habits so this kills two birds with one stone. No need for a habits book.

- [The Richest Man of Babylon](https://www.goodreads.com/book/show/1052.The_Richest_Man_in_Babylon): Simplest books on personal finance anyone can read.

- [You Are Now Less Dumb](https://www.goodreads.com/book/show/16101143-you-are-now-less-dumb): I think it has good rules about recognizing personal biases. This topic could be much more profound but this book gets the job done.

- [Networking for Systems Administrators (IT Mastery Book 5)](https://www.amazon.com/gp/product/0692376941/): TCP is one of the most fundamental layers of the modern age. Understanding it in a better way would be hugely beneficial for my career.

- [Clean Code Collection](https://www.amazon.com/Robert-Martin-Clean-Code-Collection-ebook/dp/B00666M59G): Simply put. I want to be a good software developer.

- [Incerto](https://www.amazon.com/Incerto-Fooled-Randomness-Procrustes-Antifragile/dp/0399590455): Naval said about Nassim: "Nassim is one of the very few, living, natural philosophers. His work will be the work that will last 1000 years."

- [Game Theory 101](https://www.amazon.com/Game-Theory-101-Complete-Textbook-ebook/dp/B005L7ANWC): I think this will help me understand a bit more about how to design better systems and to notice with more ease systems that need improvements.

I will try to read these books at least twice this year so that I can really absorb their contents. I will try to write book reviews so that I can absorb it even more. I will try to read science fiction books in the mix such as The Three Body problem, Snow Crash and Foundation Series as a reward.

## To Learn

If I will learn about something then it will be about:

- [GraphQL](https://graphql.org/) and use [Postgraphile](https://www.graphile.org/postgraphile/).
- ZK proofs
- Rust

We shall see how I do in a year from now.
