---
title: Installing NixOS
date: 2019-11-16
---

NixOS is purely functional linux distribution.

<!-- more -->

I have been using NixOS for over 3 weeks now and to be honest it has been extremely pleasant. I feel extremely safe doing extreme commands in my terminal. Well, not so extreme but let me show why it is amazing. I also believe that NixOS is capable of removing much of the [overload of management templates](https://blog.cedriccharly.com/post/20191109-the-configuration-complexity-curse/) that plague our servers. The utopia seems to be that we all want reproducible builds. Docker gets us halfway by making sure a team has the same environment, but it still doesn't reach everyone evenly. Due NixOS being [purely functional](https://nixos.org/~eelco/pubs/nixos-jfp-final.pdf) we have a guarantee that anything that is built by `nix` will be capabale of being built exactly the same in another machine and also in another time... so long as the build dependencies can be downloaded and don't dissapear from [archive.org](archive.org).

Lets say I want to develop an application in rust. I'm currently using `stable` but the application I want to compile requires nightly. One could create a reproducible environment for `stable` using the following command:

```
nix-shell
```
and the contents of the `shell.nix` are:
```
# shell.nix
with import <nixpkgs> { };

stdenv.mkDerivation {
  name = "romodoro";
  buildInputs = [
    pkgconfig
    rustup
  ];
}
```

Nix also has a package management tool that is completely functional (meaning all packages can be rebuilt exactly the same way all the time).


Well, I think the experience of installing NixOS is very much similar to installing ArchLinux but less configuring files. Why? Because everything related to NixOS exists in a single file called `/etc/nixos/configuration.nix`. And I mean everything. Your whole system is defined by that file and it works quite beautifully. Docker is enabled by that file. Your networking configuration is determined by that file. Here is a section of that file that I think gets the message accross:
```
    hardware = {
      opengl = {
        enable = true;
        driSupport = true;
        driSupport32Bit = true;
      };
      enableRedistributableFirmware = true;
      acpilight.enable = true;
      ledger.enable = true;
      cpu.amd.updateMicrocode = true;
      pulseaudio = {
        enable = true;
        package = pkgs.pulseaudioFull;
        support32Bit = true;
        extraConfig = ''
          load-module module-echo-cancel
        '';
      };
    };
```

If you want to look at more configuration files take a look here: [dotfiles/configuration.nix](https://gitlab.com/sebohe/dotfiles/blob/master/nixos/configuration.nix)
