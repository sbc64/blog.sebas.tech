---
title: Diffie–Hellman nginx
date: Sun 08 Sep 2019 09:51:58 PM UTC
group: nginx
---

Diffie-Hellman for nginx

 <!-- more -->

The Diffie-Hellman key exchange parameter in nginx usually a file named `dhparams.pem` that provides better security during the exchage of the ssl encryption keys. One can generate this file with the following command:

```bash
openssl dhparam -out dhparam.pem 4096
```

4096 bits is recommend to avoid the logjam attack: [https://weakdh.org/](https://weakdh.org/). This file includes the prime **p** and the generator **g**. **IT IS SAFE TO MAKE THIS FILE PUBLIC** You can learn more here: [https://www.youtube.com/watch?v=NmM9HA2MQGI](https://www.youtube.com/watch?v=NmM9HA2MQGI)
