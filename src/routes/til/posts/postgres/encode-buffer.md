---
title: Encode Postgres Buffer bytea1
date: 2021-10-08
group: postgres
---

 <!-- more -->

```sql
SELECT encode(my_column::bytea, 'hex') FROM my_table;
```
