---
title: UTF8 bit sequences
date: 2020-02-21
group: standards
---

 <!-- more -->

How does UTF-8 to multi-byte encoding? With bit sequences!

    If a byte starts with 110 it means we'll need two bytes
    If a byte starts with 1110 it means we'll need three bytes
    If a byte starts with 11110 it means we'll need four bytes
    If a byte starts with 10, it means it's a continuation of a multi-byte character sequence.
