---
title: Escape square brackets
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: ansible
---

To escape brackets in ansible one needs to use the following syntax:

 <!-- more -->

To escape brackets in ansible one needs to use the following syntax:

```yaml
- name: Get info on docker host and list images
  shell: "docker ps --format {{ '{{'}}.Names{{'}}' }}:"
  register: result
```
