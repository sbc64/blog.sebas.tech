---
title: Nix generators in lib
date: 2024-09-23
group: nix
---

 <!-- more -->

 There is a list of nice generators in the `nixpkgs` library.

```bash
nix-repl> :l <nixpkgs>
Added 22708 variables.

nix-repl> lib.generators.
lib.generators.mkDconfKeyValue         lib.generators.toDconfINI              lib.generators.toINIWithGlobalSection  lib.generators.toPlist
lib.generators.mkKeyValueDefault       lib.generators.toDhall                 lib.generators.toJSON                  lib.generators.toPretty
lib.generators.mkLuaInline             lib.generators.toGitINI                lib.generators.toKeyValue              lib.generators.toYAML
lib.generators.mkValueStringDefault    lib.generators.toINI                   lib.generators.toLua                   lib.generators.withRecursion
```

I recently used `.toGitINIT` combined with `xdg.config.<name>` to create a `git-config` file written in `nix`.
