---
title: buildInputs vs nativeBuildInputs
date: 2022-08-5
group: nixos
---

 <!-- more -->

I always forget the different between the type of build inputs in a nix derivation.

Here is my explanation.

`buildInputs`: are for libraries loaded during execution.

`nativeBuildInputs`: are compilation libraries

Sometimes the libraries of nativeBuildInputs are not populated correctly by pkg-config. If I ever find a solution to this I will post it here. The only working solution I have is:

```nix
LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath nativeBuildInputs;
```

### UPDATE: 2024-07-22

From https://nixos.org/manual/nixpkgs/unstable/#ssec-stdenv-dependencies-overview:

"Add dependencies to `nativeBuildInputs` if they are executed during the build:

- those which are needed on `$PATH` during the build, for example `cmake` and `pkg-config`

- setup hooks, for example `makeWrapper`

- interpreters needed by `patchShebangs` for build scripts (with the --build flag), which can be the case for e.g. `perl`

Add dependencies to `buildInputs` if they will end up copied or linked into the final output or otherwise used at runtime:

- libraries used by compilers, for example `zlib`,

- interpreters needed by `patchShebangs` for scripts which are installed, which can be the case for e.g. `perl`"

### Update: 2024-07-25 

### [Another nice explanation](https://gist.github.com/CMCDragonkai/45359ee894bc0c7f90d562c4841117b5)
