---
title: Dirty Git tree
date: 2023-09-15
group: nix
---

 <!-- more -->

 A code snippet for when the git repo has unstaged changes


```nix
system.configurationRevision = if (self ? rev) then
  self.rev
else
  throw "refuse to build: git tree is dirty";
```
