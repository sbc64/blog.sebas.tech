---
title: JQ tricks
date: 2020-01-28
group: jq
---

 <!-- more -->

Parsing multiple values of an array

```bash
curl -s http://165.22.237.124:26657/net_info | jq '.result.peers[] | .node_info.id +"@"+ .remote_ip'
```
