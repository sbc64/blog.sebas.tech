---
title: Disable sharing scree privacy indicator
date: 2020-12-11
group: firefox 
---

 <!-- more -->

```
about:config
privacy.webrtc.legacyGlobalIndicator
```
