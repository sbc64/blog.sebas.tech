---
title: Disable Webrtc
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: firefox 
---

Firefox Disable Webrtc

 <!-- more -->

Use these instructions if you wish to manually disable WebRTC:

- Type "about:config" into the address bar and hit Enter.
- Click the button "I accept the risk!".
- Type "media.peerconnection.enabled" in the search bar. Only one entry should appear.
- Right-click on the entry and choose "Toggle" to change the Value column to "false".`
