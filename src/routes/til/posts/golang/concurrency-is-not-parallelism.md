---
title: Concurrency is not parallelism
date: Sun 08 Sep 2019 09:51:58 PM UTC
group: go
---

Concurrency is not parallelism

 <!-- more -->

https://blog.golang.org/concurrency-is-not-parallelism

- Concurrency is a way to build things. The composition of independently executing processes.
- Parallelism is the simultaneous execution of multiple things, that might or might not be related. In programming, concurrency is the composition of independently executing processes, while parallelism is the simultaneous execution of (possibly related) computations.
- Concurrency is about dealing with lots of things at once.
  Parallelism is about doing lots of things at once.
