---
title: Byte handling
date: Sun 08 Sep 2019 09:51:58 PM UTC
group: go
---

Byte handling in go

 <!-- more -->

Notice the single quote

```go
b := []byte{'T', 'e', 'a', 'm'}
stringByte := []byte("Team")
for idx := 0; idx < 27; idx++ { 
    b = append(b, ' ')
}
```
