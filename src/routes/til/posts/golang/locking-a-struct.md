---
title: Mutex and locking
date: 2019-11-26
group: go
---

To add mutability protection a struct

 <!-- more -->

```go
import (
  "sync"
)

type Account struct {
   balance int64
   closed  bool
   sync.Mutex
}

 func (a *Account) Close() (payout int64, ok bool) {
   // This makes sure that only this function can change the contents
   a.Lock()
   // Afterthe function is done it will unlocked no matter what happens
   // in this function
   defer a.Unlock()
   fmt.Printf("")
   payout = a.balance
}
```
