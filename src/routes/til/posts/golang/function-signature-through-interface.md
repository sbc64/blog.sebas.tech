---
title: Interface signature for Function
date: 2020-01-24
group: go
---

 <!-- more -->

 I was playing around with how to do function signatures with an interface just to see if could be done and I came up with this:


```go
// Using an interface instead of a func(string, string) declaration
// because other chains might have different requirements
var currencyRequestMap = map[string]interface{}{
	"rin": newEthRequest,
	"gor": newEthRequest,
	"bnb": newCosmosRequest,
	"cos": newCosmosRequest,
}

func (k *Currency) sendTransaction(currencyIdentifier string, txHex string) error {
	k.log.Debug("sendTransaction")

	var request *URLRequest
	var err error
	switch currencyIdentifier {
	case "rin", "gor":
		request, err = currencyRequestMap["rin"].(func(string, []string) (*URLRequest, error))(k.rpcURL, []string{txHex})
	case "bnb", "cos":
		request, err = currencyRequestMa
  }
```

In the end it is just easier to make the switch to call the function directly, but it was a fun experiment. 😊
