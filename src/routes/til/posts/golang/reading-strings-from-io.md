---
title: Reading strings from io
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: go
---

Reading strings from io

 <!-- more -->

Reading strings from io

```go
R := io.Reader
buf := new(bytes.Buffer)
buf.ReadFrom(R)
stringValue := buf.String() // Convert to string
```
