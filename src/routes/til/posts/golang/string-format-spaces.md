---
title: String format padding
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: go
---

String format padding with spaces

 <!-- more -->

A way of adding padding with spaces. From [https://golang.org/pkg/fmt/](https://golang.org/pkg/fmt/): `"-" pad with spaces on the right rather than the left (left-justify the field)`

```go
fmt.Fprintf(W, "%-30s | MP | W | D | L | P", "Team")
```

The above pads with spaces (denoted by the `s` char) on the left with quantity of 30.
