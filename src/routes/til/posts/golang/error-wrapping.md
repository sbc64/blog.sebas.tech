---
title: Error Chains
date: 2021-11-26
group: go
---

 <!-- more -->

[https://go.dev/blog/go1.13-errors](https://go.dev/blog/go1.13-errors)

"The choice to wrap is about whether to give programs additional information so they can make more informed decisions, or to withhold that information to preserve an abstraction layer."

The new verb `%w` in `fmt.Errorf` allows for the wrapper e1 to contain error e2. e1 wraps e2. and now e1 has the `Unwrap()` function.

The new functions of `errors.Is()` and `errors.As` look at the enteir error chain.
