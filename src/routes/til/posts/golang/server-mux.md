---
title: Server Mux
date: 2021-02-10
group: go
---

 <!-- more -->


 The default server `net.http.ServerMux` (aka Http Router) from golang isn't very fast.

[https://github.com/julienschmidt/go-http-routing-benchmark#conclusions](https://github.com/julienschmidt/go-http-routing-benchmark#conclusions)
