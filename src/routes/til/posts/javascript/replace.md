---
title: Replace regex 
date: Tuesday 01 Oct 2019 13:51:58 PM UTC
group: javascript
---

String.replace in javascript

 <!-- more -->

Replace only works on one item. If you wan't to replace all chacters use regex:

```javascript
"a b c".replace(" ", "") // "ab c"
"a b c".replace(/ /g, "") // "abc"
```

🤯
