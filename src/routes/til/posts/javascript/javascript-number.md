---
title: Number constructor
date: Tuesday 01 Oct 2019 09:51:58 PM UTC
group: javascript
---

Javacript Number constructor with space char

 <!-- more -->

Javacript Number constructor with space char. Something amazing I learned today:

```javascript
Number(" ") === 0 // true
```

🤯
