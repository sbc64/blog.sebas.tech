---
title: Allow in background scripts 
date: 2024-05-03
group: macos
---

 <!-- more -->

 Some of the files in Settings -> General -> Login Items -> Allow in Background are scripts and it makes it hard to discern what they do. What follows is a way (not the fastest) to figure it out what each script is:

 - Disable all the shell scripts you don't know.
 - `sfltool dumpbtm > out`
 - Enable a scripts
 - `sfltool dumpbtm > out2`
 - `delta out out2`
 - repeat
