---
title: using `dd` with macos
date: 2023-10-27
group: macos
---

 <!-- more -->

It is pretty much just a copy paste of commands:

```bash
diskutil list # identify the right disk
diskutil unmountDisk diskX
dd if=nixos.iso of=/dev/sdX bs=4M status=progress conv=fdatasync
```

