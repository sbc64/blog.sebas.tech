---
title: Devops Terminology
date: 2020-07-18
group: devops
---

 <!-- more -->

From [Nixos Discourse](https://discourse.nixos.org/t/migrating-to-nix-in-production/4874/8)

- a way to execute automated deployments (CD): Hercules CI / Buildkite / Jenkins / …
- tool for deploying/provisioning: NixOps / Terraform / Morph / …
