---
title: Trusting cURL with Self Signed Certs
date: 2020-14-10
group: devops
---

 <!-- more -->

 Curl can be told to trust self signed certificates.

 First, download the certificate:

```bash
openssl s_client -showcerts -servername localhost -connect localhost:443 > cacert.pem
```

You can press `Ctrl-c` if the task doesn't end.

Second:

```bash
curl -v https://localhost/hello --cacert cacert.pem
```

Use the `--cacert` command option flag.
