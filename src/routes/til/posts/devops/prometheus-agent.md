---
title: Prometheus Agent Mode
date: 2024-05-31
group: devops
---

 <!-- more -->

https://prometheus.io/blog/2021/11/16/agent/

Basically, it allows prometheus to be stateless and push metrics to a global prometheus server.

![image](https://prometheus.io/assets/blog/2021-11-16/agent.png)
