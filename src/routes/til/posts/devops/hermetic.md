---
title: Hermetic
date: 2023-09-07
group: devops
---

 <!-- more -->

 Hermeticity in software builds refers to the practice of creating software packages that
 - are self-contained, with all necessary dependencies and resources included within the package.

 In other words, a hermetic build can be installed and run on any system, without requiring additional configuration or installations. This practice is becoming increasingly important as software complexity and interdependence continue to grow.
