---
title: Docker network open ports
date: 2019-11-11
group: docker
---

Sometimes when one tries to access a port of an application...

 <!-- more -->

Sometimes when one tries to access a port of an application inside of a docker container running in swarm mode one can't access it. For example:

```yml
services:
  gaiad:
    image: ${GAIAD_IMAGE}
    deploy:
      mode: replicated
      replicas: 1
    ports:
      - "0.0.0.0:26657:26657"
```

On `localhost` when one runs `curl -s localhost:26657/health` it just stalls and nothing happens.

In this case the docker swarm was using a wg0 interface as the overlay network. So the command should have been `curl -s 10.200.200.10:26657/health`

REMEMBER

Check which docker network it is being used for the `docker swarm`
