---
title: Docker volumes
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: docker
---

Docker volumes Docker volumes in `Dockerfile` files are always generated in docker compose unless the docker-compose mounts the volume to the same path.

 <!-- more -->

Docker volumes in `Dockerfile` files are always generated in docker compose unless the docker-compose mounts the volume to the same path.