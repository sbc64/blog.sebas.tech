---
title: Re-tagging a docker image
date: 2020-01-22
group: docker
---

 <!-- more -->

 I recently had a large docker image that took for ever to build. I didn't want to rebuild it and I also could not push it docker hub because of the main tag not belonging to me. So what I did is this:

 ```bash
➜ docker tag binance/binance-node:latest sebohe/binance:latest
➜ docker push sebohe/binance:latest
 ```

 and voilá!
