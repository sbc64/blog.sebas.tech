---
title: Obtain the keys for "--format" flag
date: 2020-06-26
group: docker
---

 <!-- more -->

To find out what data can be printed, show all content as json:

```bash
docker container ls --format='{{json .}}'
```

