---
title: Docker data directory
date: 2019-11-11
group: docker
---

One can change the default location of the docker files with:

 <!-- more -->

Edit the `docker.service`. You can find out by the path of the systemd file by running `systemc status docker` and it should have the path at the top of the output.

Then change the line starting with `ExecStart:` which has the `dockerd` command. Add the --data-root flag to make it look this now:

`ExecStart: /usr/bin/dockerd --data-root /mnt/docker -H fd:// --containerd=/run/containerd/containerd.sock`

