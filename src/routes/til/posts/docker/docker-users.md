---
title: Docker User in Containers
date: 2021-07-23
group: docker
---

 <!-- more -->

[https://docs.docker.com/engine/reference/run/#user](https://docs.docker.com/engine/reference/run/#user)

> Those users are accessible by name. When passing a numeric ID, the user does not have to exist in the container.

So basically, if you pass:

```
docker run -u myuser:myuser busbox
```

The user `myuser` doesn't exist in the docker container so it has to be created inside the docker container.

But if you instead pass the `uid` and `gid` the username only has to exist in the host system.

```
docker run -u 1000:1000 busbox
```

