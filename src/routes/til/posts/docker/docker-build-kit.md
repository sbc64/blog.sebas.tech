---
title: Docker build kit
date: 2020-05-14
group: docker
---

 <!-- more -->

 I just discovered that there is environment variable that is useful for debugging `ARG` values of a Dockerfile


 ```bash
 DOCKER_BUILDKIT=1
 ```

