---
title: Slices
date: 2019-11-13
group: rust
---

Slice variables are like arrays but their size is not known at compile time.

 <!-- more -->

```rust
// This makes an array
let a = [1, 2, 3, 4, 5];
// This makes a slice, notice the space around the ..
let nice_slice = &a[1 .. 4];
```
