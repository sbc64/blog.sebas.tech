---
title: Arrays
date: 2019-11-13
group: rust
---

Array variables

 <!-- more -->

This is how to declare an array with a size and type:

```rust
// This determines the type and size
let a: [i32; 5] = [1, 2, 3, 4, 5];
// This determines repeating elements
let a = [3; 5];
// [3, 3, 3, 3, 3]
```
