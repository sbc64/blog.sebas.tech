---
title: Unwrap vs Expect
date: 2021-11-10
group: rust
---

 <!-- more -->

Image credit to [Jake Dawkins](https://twitter.com/jakedawkins)

from [https://jakedawkins.com/2020-04-16-unwrap-expect-rust/](https://jakedawkins.com/2020-04-16-unwrap-expect-rust)

![](https://web.archive.org/save/https://jakedawkins.com/static/05366ba374cc39e323e1a7cd78647fc1/fcda8/unwrap-expect-grid.png "Selection Matrix")
