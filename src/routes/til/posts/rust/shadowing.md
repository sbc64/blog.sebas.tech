---
title: Shadowing a variable
date: 2019-11-13
group: rust
---

Shadowing variables

 <!-- more -->

This is knowing as shadowing:

```rust
let x = 5;
let x = x + 1;
let x = x * 2;
```
