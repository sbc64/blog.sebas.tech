---
title: Expressions VS Statements
date: 2019-11-13
group: rust
---

Examples

 <!-- more -->

Statements are instructions that perform some action and do not return a value.

Expressions evaluate to a resulting value.

```rust
// Expression
fn main() {
    let condition = true;
    // this is if is an expression
    let number = if condition {
        5
    } else {
        6
    };

    // this is a stament
    println!("The value of number is: {}", number);
}
```

Remember that blocks of code evaluate to the last expression in them, and numbers by themselves are also expressions. In this case, the value of the whole if expression depends on which block of code executes
