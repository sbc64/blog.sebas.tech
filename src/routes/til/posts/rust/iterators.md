---
title: Iterators
date: 2019-11-13
group: rust
---

Comment about Iterators

 <!-- more -->

There's a Rust equivalent here - people wonder why everything's an iterator - they have this expectation that operations should operate on in-memory collections. So they are too much in a hurry to collect iterators into maps or vectors, and work with those with the for-loops they learned in their childhood.
