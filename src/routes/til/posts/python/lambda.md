---
title: Using lambdas
date: 2022-06-30
group: python
---

 <!-- more -->

I used a lambda function today for the first in `python`. I have to thank `nix` for teaching me how to read functional programming. 😁

```python
data = sorted(data, key=lambda item: datetime.datetime.strptime(item[2], "%Y-%m-%d"))

list(map(lambda path: transcribePDF(path, writer), files))

```
