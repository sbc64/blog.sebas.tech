---

title: CoC keyboard shortcuts
date: 2020-11-12
group: vim
---


 <!-- more -->

<div class="table-wrapper-paragraph"><table>
<thead>
<tr>
<th>cmd</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>gd</code></td>
<td>Go to Definition</td>
</tr>
<tr>
<td><code>gy</code></td>
<td>Type Definition</td>
</tr>
<tr>
<td><code>gi</code></td>
<td>Implentation</td>
</tr>
<tr>
<td><code>gr</code></td>
<td>Show References</td>
</tr>
<tr>
<td><code>rn</code></td>
<td>Rename</td>
</tr>
<tr>
<td><code>Space qf</code></td>
<td>Quick fix</td>
</tr>
<tr>
<td><code>Shift K</code></td>
<td>Show Hover</td>
</tr>
<tr>
<td><code>Space bl</code></td>
<td>Show Buffers</td>
</tr>
<tr>
<td><code>Space e</code></td>
<td>Show Files</td>
</tr>
<tr>
<td><code>Space p</code></td>
<td>Fuzzy find</td>
</tr>
<tr>
<td><code>ctrl+b \</code></td>
<td>Split pane horz 25%</td>
</tr>
<tr>
<td><code>ctrl+b h</code></td>
<td>Jump to pane left</td>
</tr>
<tr>
<td><code>ctrl+b j</code></td>
<td>Jump to pane down</td>
</tr>
<tr>
<td><code>ctrl+b k</code></td>
<td>Jump to pane up</td>
</tr>
<tr>
<td><code>ctrl+b l</code></td>
<td>Jump to pane right</td>
</tr>
<tr>
<td><code>gs</code></td>
<td>GitStatus</td>
</tr>
</tr>
</tbody>
</table></div>

