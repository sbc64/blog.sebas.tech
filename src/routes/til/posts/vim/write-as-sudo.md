---

title: Write as sudo
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: vim
---

Filler text

 <!-- more -->

Write as sudo I always forget this so I'm writing it down

```
:w !sudo tee %
```

One can add this to `.vimrc` to use w!! as an alias 

```
cmap w!! w !sudo tee > /dev/null %
```
