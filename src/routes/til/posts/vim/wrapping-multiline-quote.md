---

title: Wrapping multiline in " (or any character)
date: 05-10-2023
group: vim
---

 <!-- more -->

` :%s/./"&"/c`

`.` is for each line

The string we use to replace the matched pattern is "&", where & stands for whatever was matched by the pattern.

