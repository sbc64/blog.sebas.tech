---

title: Select column vertically
date: 2020-11-25
group: vim
---

 <!-- more -->

Basically it is just **ctr+v** and move up and down. 🤯
