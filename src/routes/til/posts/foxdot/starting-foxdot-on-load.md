---
title: Starting foxdot on load
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: foxdot
---

Starting FoxDot on start

 <!-- more -->

After installing super collider and installing FoxDot using `Quarks.install("FoxDot")`:

```bash
$ echo FoxDot.start >> ~/.config/SuperCollider/startup.scd
```
