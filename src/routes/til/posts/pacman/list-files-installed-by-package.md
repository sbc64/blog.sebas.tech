---

title: List files installed by package
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: pacman

---

List files installed by package

 <!-- more -->

To retrieve a list of the files installed by a package: 

```bash
$ pacman -Ql package_name
```

To retrieve a list of the files installed by a remote package: 

```bash
$ pacman -Fl package_name

