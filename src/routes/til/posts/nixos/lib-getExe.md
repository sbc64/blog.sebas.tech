---
title: lib.getExe
date: 2024-06-6
group: nixos
---

 <!-- more -->

Useful function that avoids writing the path of the binary of the derivation

```
#old
"${pkgs.go_1_21}/bin/go"
#new
lib.getExe pkgs.go_1_21
```
