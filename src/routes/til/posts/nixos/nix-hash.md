---
title: Calculating Nix Hash
date: 2021-02-12
group: nixos
---

 <!-- more -->

[Source](https://gist.github.com/boxofrox/d8a3080fbb03f84b7d7a31e102b35f09)

```bash
nix-hash --to-base32 $(sha256sum go.mod| cut -f1 -d' ') --type sha256
```
