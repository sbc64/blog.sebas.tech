---
title: Propagating flake inputs
date: 2023-09-25
group: nixos
---

 <!-- more -->

You can propopagate the flake itself into other modules of nixos by using `specialArgs`:

```nix
inputs.nixos-stable.lib.nixosSystem {
    inherit system;
    specialArgs = {
        inherit self inputs;
    };
    ...
    modules = [
        ./modules
    ];
};
...


Now any module in ./modules can have as input attribute `self` and `inputs`
