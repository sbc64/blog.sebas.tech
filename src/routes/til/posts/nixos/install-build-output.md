---
title: installing build output
date: 2020-12-08
group: nixos
---

 <!-- more -->

Result usually exists after a `nix-build` command.

```nix
nix-env --install ./result
```
