---
title: C_INCLUDE_PATH vs CPLUS_INCLUDE_PATH
date: 2023-08-18
group: nixos
---

 <!-- more -->

I should have been smarter about this, so I'm writing this TIL to hopefully make myself remember that:

1. When you are working with C++ (cxx) libraries, you need to set the `$CPLUS_INCLUDE_PATH`.

2. That `$C_INCLUDE_PATH` is different to `$CPLUS_INCLUDE_PATH`. The first one is for c and the second one is for c++.
