---
title: Linux Builder Darwin lost connection
date: 2023-10-30
group: nixos
---

 <!-- more -->

 When you get a bunch of errors from doing `nix develop --command reload`:

```bash
warning: error: unable to download 'https://cache.nixos.org/0jdyaqk555dn24vbv5qx6zhyq96jq0c1.narinfo': SSL connect error (35); retrying in 338 ms
warning: error: unable to download 'https://cache.nixos.org/193mhicc920gj29wcl71pz789sms5v1w.narinfo': SSL connect error (35); retrying in 348 ms
warning: error: unable to download 'https://cache.nixos.org/04r4i4ksvcy7bk1cmlkwb8cxczc58s77.narinfo': SSL connect error (35); retrying in 352 ms
warning: error: unable to download 'https://cache.nixos.org/462960nvkjdxgndq3bvr06y7qil5qyjr.narinfo': SSL connect error (35); retrying in 320 ms
warning: error: unable to download 'https://cache.nixos.org/15hmv0kxxllpy9x035r4d8rfwvfahbp1.narinfo': SSL connect error (35); retrying in 675 ms
warning: error: unable to download 'https://cache.nixos.org/1fclfqxwlrnwxw57icqwnmzzk7lqfjwy.narinfo': SSL connect error (35); retrying in 271 ms
warning: error: unable to download 'https://cache.nixos.org/2y1vc61yg13rn7hfd0vvxlfr736cj7pk.narinfo': SSL connect error (35); retrying in 558 ms
warning: error: unable to download 'https://cache.nixos.org/45307rq4fz0cy70f1s07wpy82rd8shy3.narinfo': SSL connect error (35); retrying in 271 ms
warning: error: unable to download 'https://cache.nixos.org/2vrg83mr0rvwr1ywa7fgsg1gdwiazzvw.narinfo': SSL connect error (35); retrying in 339 ms
warning: error: unable to download 'https://cache.nixos.org/32hhwkpaxncpk49qgczb48kp6j6mz6f5.narinfo': SSL connect error (35); retrying in 309 ms
warning: error: unable to download 'https://cache.nixos.org/1h5wnvxvjc6b04hsjqildrqr5659zs09.narinfo': SSL connect error (35); retrying in 656 ms
warning: error: unable to download 'https://cache.nixos.org/05iwfphkjhqv6clnii6z8c4xhdpy2av2.narinfo': SSL connect error (35); retrying in 319 ms
warning: error: unable to download 'https://cache.nixos.org/2syab7xmdpiybx1zxa3xr1f69bd0c28h.narinfo': SSL connect error (35); retrying in 525 ms
warning: error: unable to download 'https://cache.nixos.org/21zi5jxs73z8cvz5fw0jxgcyar7npn1n.narinfo': SSL connect error (35); retrying in 276 ms
warning: error: unable to download 'https://cache.nixos.org/2rys3ahpn511pqj690l0skca3c9a7iv1.narinfo': SSL connect error (35); retrying in 282 ms
warning: error: unable to download 'https://cache.nixos.org/04r4i4ksvcy7bk1cmlkwb8cxczc58s77.narinfo': SSL connect error (35); retrying in 568 ms
warning: error: unable to download 'https://cache.nixos.org/0bfhjz09lsy8kfmk1klxkjf8lqrvl44z.narinfo': SSL connect error (35); retrying in 286 ms
warning: error: unable to download 'https://cache.nixos.org/462960nvkjdxgndq3bvr06y7qil5qyjr.narinfo': SSL connect error (35); retrying in 606 ms
```

Check that the `linux-builder` has internet connection: `ssh linux-builder ping cache.nixos.org`

If it doesn't work, restart: *`sudo launchctl kickstart -k system/org.nixos.linux-builder`*
