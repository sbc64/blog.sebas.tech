---
title: Development libraries in Nix
date: 2020-12-08
group: nixos
---

 <!-- more -->

 A lot of build input for libraries in nix need to have a pkgconfig set up.

 It isn't clear how to use it but it is important to know that there are several packages that have .dev attritibute such as:

```nix
{ pkgs ? import <nixpkgs> {} }:
let
  pkgPath = "${pkgs.glib.dev}/lib/pkgconfig:${pkgs.cairo.dev}/lib/pkgconfig:${pkgs.pango.dev}/lib/pkgconfig:${pkgs.harfbuzz.dev}/lib/pkgconfig";
in
pkgs.rustPlatform.buildRustPackage rec {
  pname = "penrose";
  version = "0.1.0";
  src = ./.;
  cargoSha256 = "xXwAK/ZwgPgjKIuYvXa5pUVqUsTv186IlkK7SSYNd3c=";
  nativeBuildInputs = with pkgs; [
    glib.dev
    cairo.dev
    pango.dev
    harfbuzz.dev
    pkg-config
    python3
    xorg.libxcb.dev
    xorg.libXrandr.dev
    xorg.libXrender.dev
  ];
  buildInputs = nativeBuildInputs;
  LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath nativeBuildInputs;
  PKG_CONFIG_PATH= pkgPath;
  # test result: FAILED. 88 passed; 13 failed; 0 ignored; 0 measured; 0 filtered out
  doCheck = false;
}
```

Lookg at the `.dev` attribute.

