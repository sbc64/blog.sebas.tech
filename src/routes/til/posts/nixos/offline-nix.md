---
title: Completely offline nix store
date: 2022-06-19
group: nixos
---

 <!-- more -->

source: https://web.archive.org/web/20220114175246/https://discourse.nixos.org/t/offline-build-source-closure/14503/8

```bash
# the nixpkgs etc. that are needed at evaluation time, all stored in the NIV json doc
evaluationPaths=$(nix-instantiate --eval --strict -E "builtins.map builtins.toString (builtins.attrValues (import ./nix/sources.nix {}))" | jq -r .[])

# all source code tarballs that result from the set of derivations that is emitted by default.nix
sourceClosurePaths=$(nix show-derivation -r $(nix-instantiate default.nix) | jq -r 'to_entries[] | select(.value.outputs.out.hash != null) | .key' | xargs nix-store -r)

nix-store --export $evaluationPaths $sourceClosurePaths > all-source-tarballs.closure

```
