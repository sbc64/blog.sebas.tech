---
title: Encrypting a binary file with agenix
date: 2024-09-23
group: nix
---

 <!-- more -->

 Copied from the (#agenix)[#agenix:nixos.org] chat.

 /madonius[er|he]
> Cheers, how would you go about encrypting a binary file with agenix?

eyJhb
> `cat mywallpaper.jpg | agenix -e somefile.age`
