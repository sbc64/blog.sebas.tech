---
title: Clang llvm
date: 2021-02-12
group: nixos
---

 <!-- more -->

You probably just need cmake in your builkd inputs for:

```
C compiler (gcc) not installed. Aborting.
```

And environment variables you need:

```
LIBCLANG_PATH = "${pkgs.llvmPackages.libclang}/lib";
LD_LIBRARY_PATH = "${pkgs.lib.makeLibraryPath buildInputs}";
```

In your nix derivation
