---
title: Environment vars in nix shell
date: 2020-02-27
group: nixos
---

 <!-- more -->

Example of how to setup environment variables for a [split package](https://hydra.nixos.org/build/34390075/download/1/nixpkgs/manual.html#idm140737319251568): 

```nix
with import <nixpkgs> { };

stdenv.mkDerivation {
  name = "eands";

  buildInputs = [
    rustup
    openssl_1_0_2
  ];
  OPENSSL_INCLUDE_DIR="${openssl_1_0_2.dev}/include/openssl";
  OPENSSL_LIB_DIR="${openssl_1_0_2.out}/lib";
  OPENSSL_ROOT_DIR="${openssl_1_0_2.out}";
}
```
