---
title: User Groups with NixOS
date: 2022-06-07
group: nixos
---

 <!-- more -->

User groups are not created automatically with:

```nix
users.users."dendrite" = {
  group = "dendrite";
  isSystemUser = true;
};
```

```bash
# id dendrite
uid=996(dendrite) gid=65534(nogroup) groups=65534(nogroup)
```

You need:

```nix
users.groups.dendrite = {};
users.users."dendrite" = {
  group = "dendrite";
  isSystemUser = true;
};
```

which gives you:

```bash
# id dendrite
uid=996(dendrite) gid=994(dendrite) groups=994(dendrite)
```
