---
title: Nix fileSytem mounting by gid
date: 2023-10-27
group: nixos
---

 <!-- more -->


```nix
  fileSystems."${dataDirPath}" = {
    device = "/dev/disk/by-id/scsi-0HC_Volume_100019012";
    options = ["gid=${builtins.toString config.users.groups.nextcloud.gid}"];
  };
```
