---
title: nixos-anywhere command
date: 2024-05-30
group: nixos
---

 <!-- more -->
```
nix run github:nix-community/nixos-anywhere -- --flake .#enticed-cow root@<ip> --build-on-remote --copy-host-keys
```
