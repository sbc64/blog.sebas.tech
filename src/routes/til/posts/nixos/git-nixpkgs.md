---
title: Using the nixpkgs packages
date: 2021-03-11
group: nixos
---

 <!-- more -->

To install a package form the nixpkgs repo use:

```bash
nix-env -i obinskit -I nixpkgs=./
```

and the directory should have the contents of https://github.com/NixOS/nixpkgs/
