---
title: Flake Inputs URL spec
date: 2022-08-09
group: nixos
---

 <!-- more -->

Here are some examples of flake references in their URL-like representation:

- .: The flake in the current directory.
- /home/alice/src/patchelf: A flake in some other directory.
- nixpkgs: The nixpkgs entry in the flake registry.
- nixpkgs/a3a3dda3bacf61e8a39258a0ed9c924eeca8e293: The nixpkgs entry in the flake registry, with its Git revision overridden to a specific value.
- github:NixOS/nixpkgs: The master branch of the NixOS/nixpkgs repository on GitHub.
- github:NixOS/nixpkgs/nixos-20.09: The nixos-20.09 branch of the nixpkgs repository.
- github:NixOS/nixpkgs/a3a3dda3bacf61e8a39258a0ed9c924eeca8e293: A specific revision of the nixpkgs repository.
- github:edolstra/nix-warez?dir=blender: A flake in a subdirectory of a GitHub repository.
- git+https://github.com/NixOS/patchelf: A Git repository.
- git+https://github.com/NixOS/patchelf?ref=master: A specific branch of a Git repository.
- git+https://github.com/NixOS/patchelf?ref=master&rev=f34751b88bd07d7f44f5cd3200fb4122bf916c7e: A specific branch and revision of a Git repository.
- https://github.com/NixOS/patchelf/archive/master.tar.gz: A tarball flake.
