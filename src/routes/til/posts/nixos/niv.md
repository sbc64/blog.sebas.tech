---
title: Updating Niv
date: 2020-06-22
group: nixos
---

 <!-- more -->

[Niv](https://github.com/nmattia/niv) is a nixpkgs source manager.

To set niv to a specific nixpkgs branch use:

```bash
niv update nixpkgs -b nixos-20.03
```
