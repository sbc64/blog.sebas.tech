---
title: Custom Nixpkgs
date: 2020-11-11
group: nixos
---

 <!-- more -->

 Sometimes I want to use a custom nixpkgs for my build. According to this [post](https://discourse.nixos.org/t/documentation-of-how-to-set-up-a-nix-channel/5781/6):

```
A channel is just a folder (or a tarball) with a default.nix that returns an attrset.
```


So the way to do this is by using the `-I` flag.

```bash
nix-build -I nixpkgs=../nixpkgs openethereum-docker.nix
```
