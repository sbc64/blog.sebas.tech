---
title: Get a derivation from a store path
date: 2020-09-01
group: nixos
---

 <!-- more -->

```bash
nix show-derivation `nix path-info --derivation /nix/store/6ap82w....`
```
