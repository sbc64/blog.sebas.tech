---
title: Home Manager
date: 2023-05-16
group: nixos
---

 <!-- more -->

You can self reference home manager variables like this:

```nix
let
   settings = config.home.file."Library/Application Support/Code/User/settings.json";
in {}
```
