---
title: NixOS Generation
date: 2020-06-22
group: nixos
---

 <!-- more -->

This lists the generations of NixOS

```bash
sudo nix-env --list-generations --profile /nix/var/nix/profiles/system
```
