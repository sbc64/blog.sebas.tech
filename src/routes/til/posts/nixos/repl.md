---
title: Nix Repl + Flakes
date: 2023-05-16
group: nixos
---

 <!-- more -->

you can load flakes and build the `outputs` of it with the following:

```bash
nix repl
:lf .
:b outputs.<desired output>
```
