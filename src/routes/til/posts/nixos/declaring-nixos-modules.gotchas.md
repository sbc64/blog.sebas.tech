---
title: Declaring NixOS modules gotchas
date: 2020-12-08
group: nixos
---

 <!-- more -->

 Just a compilation of gotchas I have bumped into.

```nix
{
lib,
...
}: with lib; {
    # You need to have a larger attribute name,
    such as "zfs-root" or "within"
    options.zfs-root.boot.enable = mkEnableOption "Enable zfs-root boot option";
    # This one won't work!
    options.zfs-root.enable = mkEnableOption "Enable zfs-root boot option";
}
```

