---
title: Reducing internet download of nix flakes
date: 2022-03-08
group: nixos
---

 <!-- more -->

From: [https://ianthehenry.com/posts/how-to-learn-nix/new-profiles/](https://ianthehenry.com/posts/how-to-learn-nix/new-profiles/)

Add this to `~/.config/nix/nix.conf`:

```
flake-registry = /home/sebas/.dotfiles/flake-registry.json
```

run: `nix registry pin nixpkgs`
