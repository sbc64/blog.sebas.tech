---
title: Flakes Custom Nixpkgs
date: 2024-05-29
group: nixos
---

 <!-- more -->


```

# part of flake.nix
{
  inputs = {...};
  let
    unstable = import nixpkgs-unstable {...};
    legacy = import nixpkgs-legacy {...};
  in
    nixosConfigurations.laptop = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = { inherit inputs, unstable, legacy; };
      modules = [
        ./configuration.nix
      ];
}

# configuration.nix
{self, inputs, pkgs, unstable, legacy, ...} : {
  environment.systemPackages = [ unstable.foo legacy.bar pkgs.baz ];
}

```
