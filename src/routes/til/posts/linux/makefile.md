---
title: Makefile VPATH
date: Sun 08 Sep 2019 09:51:58 PM UTC
group: linux
---

Makefile VPATH

 <!-- more -->

 Makefile relies on files being built. The VPATH ensures that `make` checks for files at a certain path.


```make
VPATH flags=.makeFlags
VPATH=$(flags)
$(shell mkdir -p $(flags))
test: touch
    $(flags)/$@
```

Running the above twice will result in:

```
$ make test touch .makeFlags/test
$ make test make: '.makeFlags/test' is up to date.
```
