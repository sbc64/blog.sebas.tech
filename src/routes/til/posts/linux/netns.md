---
title: Linux network namespace
date: 2020-10-09
group: linux
---

 <!-- more -->

```
ip link set netns private dev wg3
```

netns stands for network namespace
