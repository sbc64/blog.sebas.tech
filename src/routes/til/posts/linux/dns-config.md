---
title: Linux dns config
date: Sun 08 Sep 2019 09:51:58 PM UTC
group: linux
---

DNS config

 <!-- more -->

The default dns resolver for systemd is systemd-resolved. It's configuration is located at: `/etc/systemd/resolved.conf`

One can specify which dns to use in `/etc/NetworkManager/NetworkManager.conf`:

```
# /etc/NetworkManager/NetworkManager.conf
[main] dns=dnsmasq # or none
```
