---
title: Ulimit values
date: 2021-01-25
group: linux
---

 <!-- more -->

<table>
<thead>
<tr>
<th>Item Keyword</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>core</td>
<td>limits the core file size (KB)</td>
</tr>
<tr>
<td>data</td>
<td>max data size (KB)</td>
</tr>
<tr>
<td>fsize</td>
<td>maximum filesize (KB)</td>
</tr>
<tr>
<td>memlock</td>
<td>max locked-in-memory address space (KB)</td>
</tr>
<tr>
<td>nofile</td>
<td>max number of open file descriptors</td>
</tr>
<tr>
<td>rss</td>
<td>max resident set size (KB)</td>
</tr>
<tr>
<td>stack</td>
<td>max stack size (KB)</td>
</tr>
<tr>
<td>cpu</td>
<td>max CPU time (MIN)</td>
</tr>
<tr>
<td>nproc</td>
<td>max number of processes</td>
</tr>
<tr>
<td>as</td>
<td>address space limit (KB)</td>
</tr>
<tr>
<td>maxlogins</td>
<td>max number of logins for this user</td>
</tr>
<tr>
<td>maxsyslogins</td>
<td>max number of logins on the system</td>
</tr>
<tr>
<td>priority</td>
<td>the priority to run user process with</td>
</tr>
<tr>
<td>locks</td>
<td>max number of file locks the user can hold</td>
</tr>
<tr>
<td>sigpending</td>
<td>max number of pending signals</td>
</tr>
<tr>
<td>msgqueue</td>
<td>– max memory used by POSIX message queues (bytes)</td>
</tr>
<tr>
<td>nice</td>
<td>max nice priority allowed to raise to values: [-20, 19]</td>
</tr>
<tr>
<td>rtprio</td>
<td>max realtime priority</td>
</tr>
<tr>
<td>chroot</td>
<td>change root to directory (Debian-specific)</td>
</tr>
</tbody>
</table>
