---
title: Looking at boot logs
date: 2021-04-24
group: linux
---

 <!-- more -->

This set of commands allows you to see all the journalctl logs for a specific boot sequence.

```bash
journalctl --list-boots

journalctl --boot=0 # this is usually the latest boot
```
