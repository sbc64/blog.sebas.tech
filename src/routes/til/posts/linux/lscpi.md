---
title: LSPCI show verbose log
date: 2024-04-19
group: linux
---

 <!-- more -->

Just a reminder that the `lspci` command has a verbose option:

```
# lspci -vvv
```
