---
title: zpool encryption
date: 2022-10-31
group: linux
---

 <!-- more -->

```
zpool create \
    -O mountpoint=none -o ashift=12 -O atime=off -O acltype=posixacl -O xattr=sa -O compression=lz4 \
    -O encryption=aes-256-gcm -O keyformat=passphrase -O keylocation=prompt \
    zroot ata-M900-512_AA000000000000000021-part3 ata-Samsung_SSD_870_QVO_2TB_S5SUNF0NC03781X-part3
```
