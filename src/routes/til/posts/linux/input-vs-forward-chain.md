---
title: Input vs Forward chain nftables
date: 2024-05-30
group: linux
---

 <!-- more -->
In nftables, the distinction between `input` and `forward` rules lies in how packets are processed based on their destination. Here is a detailed explanation:

### Input Chain
- **Purpose**: The `input` chain is used to filter packets that are destined for the local system itself. These are packets whose final destination is the machine on which nftables is running.
- **When It's Used**: If a packet is addressed to one of the IP addresses of the local machine and is intended to be processed by a local application (e.g., a web server, SSH daemon), it will traverse the `input` chain.
- **Example Rules**:
  - Allow incoming SSH connections to the local machine:
    ```nft
    chain input {
        type filter hook input priority filter; policy accept;
        tcp dport 22 accept
    }
    ```

### Forward Chain
- **Purpose**: The `forward` chain is used to filter packets that are being routed through the local machine to another destination. These packets are not meant for the local system but are simply passing through it to reach another machine.
- **When It's Used**: If the local machine is acting as a router or gateway, packets that need to be forwarded to another network interface or IP address will traverse the `forward` chain.
- **Example Rules**:
  - Allow forwarding of HTTP traffic:
    ```nft
    chain forward {
        type filter hook forward priority filter; policy accept;
        tcp dport 80 accept
    }
    ```

### Example Scenario
Consider a machine with nftables acting as both a server and a router:

- **Input Chain**: Used to control access to services on the local machine. For example, allowing SSH traffic destined for the local machine:
  ```nft
  chain input {
      type filter hook input priority filter; policy accept;
      ip daddr 192.168.1.1 tcp dport 22 accept
  }
  ```
  In this example, the rule accepts incoming SSH connections to the IP address `192.168.1.1` on the local machine.

- **Forward Chain**: Used to control traffic passing through the machine from one network interface to another. For example, allowing HTTP traffic to be forwarded:
  ```nft
  chain forward {
      type filter hook forward priority filter; policy accept;
      ip daddr 192.168.2.0/24 tcp dport 80 accept
  }
  ```
  In this example, the rule allows forwarding of HTTP traffic to the network `192.168.2.0/24`.

### Summary
- **Input Chain**: Filters traffic destined for the local machine.
- **Forward Chain**: Filters traffic that is being routed through the local machine to another destination.

Understanding these differences is crucial for configuring nftables correctly to manage both local and transit traffic.
