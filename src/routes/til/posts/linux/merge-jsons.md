---

title: Merge jsons
date: Sun 08 Sep 2019 09:51:58 PM UTC
group: linux

---

Merge jsons

 <!-- more -->

To merge two json files one can use the following 

```bash
$ jq -s '.[0] .[1]' file1 file2 > output.json
```
The contents of file2 take precedence over the contents of file1
