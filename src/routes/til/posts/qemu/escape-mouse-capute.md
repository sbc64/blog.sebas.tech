---

title: Escape mouse capture
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: qemu

---

Escape mouse capture

 <!-- more -->

I always forget this so I'm writing this here: `Alt + Ctrl + G`
