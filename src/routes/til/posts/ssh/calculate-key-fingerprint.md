---
title: Calculate sha256 ssh key fingerprint
date: 2024-02-17
group: ssh
---


```bash
ssh-keygen -l -E sha256 -f ssh_host_ed25519_key.pub
SHA256:5SmjOVkk0MHcGKtoLPG6eHqZp/aM0OPUrRwSNUAp/aY root@sound-bunny (ED25519)
```
