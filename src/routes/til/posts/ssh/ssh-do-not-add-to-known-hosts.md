---
title: Do not add host key to `~/.ssh/known_hosts`
date: 2024-01-16
group: ssh
---

 <!-- more -->

**-o "UserKnownHostsFile=/dev/null"**

```bash
ssh root@192.168.20.25 -o "UserKnownHostsFile=/dev/null" 
```
