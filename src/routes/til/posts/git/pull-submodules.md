---
title: Pulling submodules again
date: 2019-11-12
group: git
---

Command to pull git submodules

 <!-- more -->

```bash
git submodule update --init --recursive
```
