---
title: Comparing commits to master
date: 2022-07-13
group: git
---

 <!-- more -->

The following command will show you all the diffs in the commit that differ form master branch.

```bash

git show HEAD...`git merge-base master HEAD`
```
