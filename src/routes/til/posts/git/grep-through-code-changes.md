---
title: Grep commit changes
date: 2024-03-30
group: git
---

 <!-- more -->


```bash
git grep <regexp> $(git rev-list --all)
```
