---

title: Collapse markdown
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: markdown
---

Collapse markdown

 <!-- more -->

To create collapsable markdown segments one can use the following: 

```markdown 
## collapsible markdown?
<details>
    <summary>CLICK ME</summary>
    <p> #### yes, even hidden code blocks!`python print("hello world!")`</p>
</details>`
