---

title: Recurring tasks
date: Sun 08 Sep 2019 09:51:57 PM UTC
group: taskwarrior
---

 <!-- more -->

Recurring tasks To add tasks that are recurring in task warrior in the sense that after they are done they get re added to the log automatically one needs to do the following: 

```bash
$ task add project:Daily.code recur:daily due:eod Read 1 hour of golang books
```

The `due:eod` and the `recur:daily` are the important flags.
