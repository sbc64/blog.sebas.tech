---

title: Colors for different task
date: 2019-12-05
group: taskwarrior
---

Colors for different tasks

 <!-- more -->

[RULES](https://www.systutorials.com/docs/linux/man/5-task-color/)

Taskwarrior supports colorization rules. These are configuration values that specify a color, and the conditions under which that color is used. By example, let us add a few tasks:

```bash
$ task add project:Home priority:H pay the bills
$ task add project:Home            clean the rug
$ task add project:Garden          clean out the garage
```

We can add a color rule that uses a blue background for all tasks in the Home project:

```bash
$ task config color.project.Home 'on blue'
```

We use quotes around `'on blue'` because there are two words, but they represent one value in the .taskrc file. Now suppose we wish to use a bold yellow text color for all cleaning work:

```bash
task config color.keyword.clean 'bold yellow'
```

Now what happens to task 2, which belongs to project Home (blue background), and is also a cleaning task (bold yellow foreground)? The colors are combined, and the task is shown as "bold yellow on blue".

Color rules can be applied by project and description keyword, as shown, and also by priority (or lack of priority), by active status, by being due or overdue, by being tagged, or having a specific tag (perhaps the most useful rule) or by being a recurring task.

It is possible to create a very colorful mix of rules. With 256-color support, those colors can be made subtle, and complementary, but without care, this can be a visual mess. Beware!

In such cases, consider using the 'rule.color.merge=no' option to disable the color blending.

The precedence for the color rules is determined by the configuration variable 'rule.precedence.color', which by default contains: `deleted,completed,active,keyword.,tag.,project.,overdue,scheduled,due.today,due,blocked,blocking,recurring,tagged,uda.`

These are just the color rules with the `color.` prefix removed. The rule `color.due.today` is the highest precedence, and `color.deleted` is the lowest.

The keyword rule shown here as `keyword`. corresponds to a wildcard pattern, meaning `color.keyword.*`, or in other words all the keyword rules. Similarly for the `color.tag.*` and `color.project.*` rules.

There is also `color.project.none`, `color.tag.none` and `color.pri.none` to specifically represent missing data. 