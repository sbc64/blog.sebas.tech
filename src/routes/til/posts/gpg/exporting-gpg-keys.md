---

title: Exporting gpg keys
date: Sun 08 Sep 2019 09:51:58 PM UTC
group: gpg
---

Exporting gpg keys

 <!-- more -->

Run the following to export the public key.

```bash
$ gpg --armor --export 040FE79B > voldermort.public.asc
```
