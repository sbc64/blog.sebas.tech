---

title: Basic gpg functions
date: Sun 08 Sep 2019 09:51:58 PM UTC
group: gpg
---

Basic GPG functions

 <!-- more -->

Show all the keys including subkeys fingerprints

```bash
$ gpg -K --with-subkey-fingerprints
```

Edit a key with --expert flag

```bash
$ gpg --edit-key --expert <key id>
```
