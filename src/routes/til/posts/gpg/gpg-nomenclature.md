---

title: GPG Key flag subtype
date: 2019-11-29
group: gpg
---

GPG Key Flag

 <!-- more -->

|Flag | gpg character | Description |
|-----|---------------|-------------|
|0x01|  “C” | Key Certification |
|0x02|	“S” |	Sign Data |
|0x04|	“E” |	Encrypt Communications |
|0x08|	“E” |	Encrypt Storage |
|0x10|      |	Split key |
|0x20|	“A” |	Authentication |
|0x80|		|  Held by more than one person |
