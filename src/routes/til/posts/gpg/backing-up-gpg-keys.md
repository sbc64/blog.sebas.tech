---

title: Backing up gpg keys
date: Sun 08 Sep 2019 09:51:58 PM UTC
group: gpg
---

Backing up the GPG directory

 <!-- more -->

To backup the GPG directory with restrictive access

```bash
$ umask 077; tar -cf $HOME/gnupg-backup`date +"%Y_%m_%d%H_%M_%S"`.tar -C $HOME .gnupg
```
