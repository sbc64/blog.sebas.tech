---
title: Smarter way of hanlding dns updates
date: 2021-07-02
group: aws
---

 <!-- more -->

Recently I had to updated the DNS A record to an ip address within the AWS system.

The change was to update an elatic ip address to another elastic ip address that was managed by terraform state.

The problem is that one elastic ip address can only be associated to one instance, so I couldn't use the old DNS A record address on the new instance. This would to an exprired DNS cache with the old A record while the new ip address (the one managed by tfstate) would have to wait to be propogated.

I made the mistake of waiting for dns cache which lead to roughly a downtime of 30 mins.

The better approach to make the downtime less was to:

1. Use the old DNS A record attached to the instance that will get the new elastic ip address.
2. Create a second DNS A record that points to the public ipv4 of the instance (not the elastic ip)
3. Wait for the DNS cache to have round robin dns records with the public ipv4 ip of the instance and the old elastic ip address.
4. After 30 mins, update (not a new one) the DNS A record of the elastic ip address with the new ip address (the elastic ip managed by terraform).
5. Assign the new floating ip address managed by terraform to the instance.
