---
title: SQL lite Litestream
date: 2022-11-2
group: sql
---

 <!-- more -->

https://litestream.io/how-it-works/

```
Litestream is a streaming replication tool for SQLite databases. It runs as a separate background process and continuously copies write-ahead log pages from disk to one or more replicas.
```

Note To Self: This can be a useful replication mechanism with a light weigh db. Fly.io basically replicates their consul state with litestream.